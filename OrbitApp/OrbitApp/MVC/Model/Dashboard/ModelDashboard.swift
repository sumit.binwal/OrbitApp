//
//  ModelDashboard.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 11/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import Foundation
class ModelDashboard
{
    var transactionWalletBal = ""
    var cryptoWalletBal = ""
    var tokenWalletBal = ""
    var cryptoWallet = [ModelCryptoCurrency]()
    var packageVal = ""
    var commissionEarned = ""
    var psdEarned = ""
    var totalRevenue = ""
    
}

class ModelCryptoCurrency
{

    var currentRate : String = ""
    var cointImgURL : String = ""
    var name : String = ""
    var coinQty : String = ""
    var coinUSDBal : String = ""
    var coinChange : String = ""
    
    /// update cyrpto Curreny model data
    ///
    /// - Parameter dictionary: dictionary data
    func updateCryptoCurrencyModel(usingDictionary dictionary:[String:Any]) -> Void {
        
        if let coinChng = dictionary["coinchange"] as? String {
            coinChange = coinChng
        }
        
        if let current = dictionary["coincurrentrate"] as? String {
            currentRate = current
        }
        
        if let imgUrl = dictionary["coinimageURL"] as? String {
            cointImgURL = imgUrl
        }
        
        if let coinName = dictionary["coinname"] as? String {
            name = coinName
        }
        
        if let qty = dictionary["coinqty"] as? String {
            coinQty = qty
        }

        if let bal = dictionary["coinusdbalance"] as? String {
            coinUSDBal = bal
        }

    }
}
