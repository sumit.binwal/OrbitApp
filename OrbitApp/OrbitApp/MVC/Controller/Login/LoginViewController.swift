//
//  LoginViewController.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 10/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func isValidate() -> Bool
    {
        if textFieldEmail.text!.isEmptyString()
        {
            UIAlertController .showAlertWith(title: String.MyApp.AppName, message: "Please enter email address first", dismissBloack: {})

            return false
        }
        if textFieldPassword.text!.isEmptyString()
        {
            UIAlertController .showAlertWith(title: String.MyApp.AppName, message: "Please enter password address first", dismissBloack: {})

            return false
        }
        return true
    }
    
    
    @IBAction func loginButtonClickedAction(_ sender: Any)
    {
        if isValidate()
        {
        loginAPI()
        }
    
    }
    
    @IBAction func forgotButtonClickedAction(_ sender: Any)
    {
        let forgotVC = UIStoryboard.getLoginStoryBoard().instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotVC, animated: true)
        
    }
    @IBAction func signupButtonClickedAction(_ sender: Any) {
    }
    
}

extension LoginViewController
{
    func loginAPI()
    {
        self.view.endEditing(true)

        UtilityClass.startAnimating()
        
        let urlToHit = EndPoints.login(textFieldEmail.text ?? "", textFieldPassword.text ?? "", "~M!M5vCM!MGROLD@P20!6~").path
    
        print(urlToHit)
        
        FireApi.shared().performRequest(for: urlToHit, method: .get, headers: FireApi.defaultHeaders, parameters: nil) { [weak self] result in
            
                         UtilityClass.stopAnimating()
            
                        guard let `self` = self else {return}
                        print(result)
                        switch result {
            
                        case .error(let error):
                            UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
            
                        case .success(let responseDict, let statusCode):
            
                            print(responseDict)
                            print(statusCode)
            
                            // Error
                            if statusCode == FireApi.ErrorCodes.code203.rawValue {
            
                                if let msg = responseDict["message"] as? String {
                                    UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                                }
                                else {
                                    UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                                }
            
                                return
                            }
            
                            // Success
                            if statusCode == FireApi.ErrorCodes.code200.rawValue {
            
                                    if let responseCode = responseDict["id"] as? String
                                    {
                                        if responseCode == "0"
                                        {
                                            UIAlertController .showAlertWith(title: String.MyApp.AppName, message: "Email and password not correct", dismissBloack: {})
                                        }
                                        else
                                        {
                                            UserDefaults.saveUserInformation(userInfo: responseDict)
                                            APPDELEGATE.screenRedirect()
                                        }
                                }
                            }
            
                            return
                        }
                    }
                
    }
}
