//
//  DashboardViewController.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 11/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit
import SDWebImage

class DashboardViewController: UIViewController {
    @IBOutlet weak var lblTotalPackage: UILabel!
    @IBOutlet weak var lblPSDValue: UILabel!
    @IBOutlet weak var lblTransWalletBal: UILabel!
    @IBOutlet weak var lblTokenBal: UILabel!
    @IBOutlet weak var lblCryptoWallet: UILabel!
    @IBOutlet weak var lblTotalRevenue: UILabel!
    @IBOutlet weak var lblCommissionVal: UILabel!
    @IBOutlet var vwPortfolio: UIView!
    @IBOutlet weak var tableVwDashboard: UITableView!
    @IBOutlet var vwFooter: UIView!
    var modelDashboard = ModelDashboard()
    override func viewDidLoad() {
        super.viewDidLoad()

        //Add Header View IN Table
        vwPortfolio.frame = CGRect.init(x: 0, y: 0, width: 375*scaleFactorX, height: 379*scaleFactorX)
        tableVwDashboard.tableHeaderView = vwPortfolio
        
        vwFooter.frame = CGRect.init(x: 0, y: 0, width: 375*scaleFactorX, height: 451*scaleFactorX)
        tableVwDashboard.tableFooterView = vwFooter

        
        _ = menuBarButton
        
        //TableView Delegate
        tableVwDashboard.delegate = self
        tableVwDashboard.dataSource = self
        
        dashboardMainDataAPI()
    }
    
}

extension DashboardViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelDashboard.cryptoWallet.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
        return 40 * scaleFactorX
        }
        else
        {
            return 100 * scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: Dashboard_Header_Cell_Identifier) as! DashboardHeaderCell
            return cell

        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: Dashboard_Cell_Identifier) as! DashboardTableCell
            
            if modelDashboard.cryptoWallet.count > 0
            {
                cell.lblCoinName.text = modelDashboard.cryptoWallet[indexPath.row - 1].name
                cell.lblQty.text = modelDashboard.cryptoWallet[indexPath.row - 1].coinQty
                cell.lblCurrentRate.text = "USD \(modelDashboard.cryptoWallet[indexPath.row - 1].currentRate)"
                cell.lblBalance.text = "USD \(modelDashboard.cryptoWallet[indexPath.row - 1].coinUSDBal)"
                cell.coinImgVw.sd_setImage(with: URL.init(string: modelDashboard.cryptoWallet[indexPath.row - 1].cointImgURL), completed: nil)

            }
            return cell

        }
    }
    
    
}

extension DashboardViewController
{
    func getCryptodataAPI()
    {
        self.view.endEditing(true)
        
        UtilityClass.startAnimating()
        
        let urlToHit = EndPoints.cryptoWalletData(UserDefaults.userID!, "GTVCDKO756432C5Pf@POPDF").path
        
        
        print(urlToHit)
        
        FireApi.shared().performRequest(for: urlToHit, method: .get, headers: FireApi.defaultHeaders, parameters: nil) { [weak self] result in
            
            UtilityClass.stopAnimating()
            
            guard let `self` = self else {return}
            print(result)
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    if let arr = responseDict["Data"] as? [[String:Any]] {
                        self.updateArray(usingArray: arr)
                    }
                }
                
                return
            }
        }
    }
    
    func accountTurnOverDataAPI()
    {
        self.view.endEditing(true)
        
        UtilityClass.startAnimating()
        
        let urlToHit = EndPoints.dashboardTurnoverData(UserDefaults.userID!, "GTVCDKOP*@67vC5Pf@POPDF").path
        
        
        print(urlToHit)
        
        FireApi.shared().performRequest(for: urlToHit, method: .get, headers: FireApi.defaultHeaders, parameters: nil) { [weak self] result in
            
            UtilityClass.stopAnimating()

            guard let `self` = self else {return}
            print(result)
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    self.getCryptodataAPI()

                    
                    if let commission = responseDict["TotalCommissionEarned"] as? String {

                        self.lblCommissionVal.text = "USD \(commission)"

                    }
                    if let packageVal = responseDict["TotalPackagesValue"] as? String {

                        self.lblTotalPackage.text = "USD \(packageVal)"

                    }
                    if let totalRev = responseDict["TotalRevenue"] as? String {

                        self.lblTotalRevenue.text = "USD \(totalRev)"

                    }
                    if let psdEarned = responseDict["TotalPSDEarned"] as? String {
                        
                        self.lblPSDValue.text = "USD \(psdEarned)"
                        
                    }
                    
                }
                
                return
            }
        }
    }
    
    func dashboardMainDataAPI()
    {
        self.view.endEditing(true)
        
        UtilityClass.startAnimating()
        
        let urlToHit = EndPoints.dashboardMainData(UserDefaults.userID!, "8FUIPKMGT*@67vC5Pf@POPDF").path
        
        
        print(urlToHit)
        
        FireApi.shared().performRequest(for: urlToHit, method: .get, headers: FireApi.defaultHeaders, parameters: nil) { [weak self] result in
            
            UtilityClass.stopAnimating()

            guard let `self` = self else {return}
            print(result)
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    self.accountTurnOverDataAPI()

                    if let cryptoBal = responseDict["cryptowalletbalance"] as? String {
                        
                        self.lblCryptoWallet.text = "USD \(cryptoBal)"
                        
                    }
                    if let tokenWallet = responseDict["tokenwalletbalance"] as? String {
                        
                        self.lblTokenBal.text = "USD \(tokenWallet)"
                        
                    }
                    if let transBal = responseDict["trasactionwalletbalance"] as? String {
                        
                        self.lblTransWalletBal.text = "USD \(transBal)"
                        
                    }
                    
                }
                
                return
            }
        }
    }
    
    /// function to update request model arr
    ///
    /// - Parameter array: array of dictionary data
    func updateArray(usingArray array: [[String:Any]]) {
        
        modelDashboard.cryptoWallet.removeAll()
        
        for dict in array {
            
            let  modelCrypto = ModelCryptoCurrency()
            modelCrypto.updateCryptoCurrencyModel(usingDictionary: dict)
            modelDashboard.cryptoWallet.append(modelCrypto)
        }
        
        tableVwDashboard.reloadData()
    }

}
