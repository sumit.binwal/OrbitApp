//
//  ForgotPasswordVC.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 11/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    @IBOutlet weak var textFieldEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitButtonClickedAction(_ sender: Any)
    {
        if isValidate()
        {
        forgotPasswordAPI()
        }
    }
    
    @IBAction func backButtonClickedAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    func isValidate() -> Bool
    {
        if textFieldEmail.text!.isEmptyString()
        {
            UIAlertController .showAlertWith(title: String.MyApp.AppName, message: "Please enter email address first", dismissBloack: {})
            
            return false
        }
        return true
    }

    
}
extension ForgotPasswordVC
{
    func forgotPasswordAPI()
    {
        self.view.endEditing(true)
        
        UtilityClass.startAnimating()
        
        let urlToHit = EndPoints.forgotPassword(textFieldEmail.text!).path
        
        
        print(urlToHit)
        
        FireApi.shared().performRequest(for: urlToHit, method: .get, headers: FireApi.defaultHeaders, parameters: nil) { [weak self] result in
            
            UtilityClass.stopAnimating()
            
            guard let `self` = self else {return}
            print(result)
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    if let responseCode = responseDict["msg"] as? String
                    {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: responseCode, dismissBloack: {})

                    }
                    else
                    {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: "Email address not correct", dismissBloack: {})

                    }
                }
                
                return
            }
        }
        
    }
}
