//
//  DashboardHeaderCell.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 11/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit
let Dashboard_Header_Cell_Identifier = "DashboardHeaderCell"

class DashboardHeaderCell: UITableViewCell {
    @IBOutlet weak var vwHeader: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = vwHeader.frame
        rectShape.position = vwHeader.center
        rectShape.path = UIBezierPath(roundedRect: vwHeader.bounds, byRoundingCorners: [.topRight , .topLeft], cornerRadii: CGSize(width: 5, height: 5)).cgPath
        
        //Here I'm masking the textView's layer with rectShape layer
        vwHeader.layer.mask = rectShape

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
