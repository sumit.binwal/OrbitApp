//
//  DashboardTableCell.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 11/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit
let Dashboard_Cell_Identifier = "DashboardTableCell"

class DashboardTableCell: UITableViewCell {

    @IBOutlet weak var coinImgVw: UIImageView!
    @IBOutlet weak var lblCoinName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblCurrentRate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
