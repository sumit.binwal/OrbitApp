//
//  SideMenuTableCell.swift
//  OrbitApp
//
//  Created by Sumit Sharma on 11/08/19.
//  Copyright © 2019 Sumit Sharma. All rights reserved.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    @IBOutlet weak var imgVwSepratorLine: UIImageView!
    @IBOutlet weak var labelText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle =  .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
