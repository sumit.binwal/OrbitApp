//
//  UIStoryBoardExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import UIKit

/// MARK: - UIStoryboard extension
extension UIStoryboard
{
    /// function to get Dashboard StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getLoginStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Login", bundle: nil)
        return storyBoard
    }
    static func getDashboardStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Dashboard", bundle: nil)
        return storyBoard
    }
    static func getSideMenuStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "SideMenu", bundle: nil)
        return storyBoard
    }

}
