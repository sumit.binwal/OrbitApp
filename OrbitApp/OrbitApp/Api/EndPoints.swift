//
//  EndPoints.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 18/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation

enum StaticEndpoints {
    
    case developmentURL
    case localURL
    case stagingURL
    case socketBaseURL
    case termsAndCondition
    case privacyAndPolicy
    case other(String)
    
    var url: URL {
        
        switch self {
            
        case .termsAndCondition:
            return URL (string: String.MyApp.TermsAndConditionURL)!
            
        case .privacyAndPolicy:
            return URL (string: String.MyApp.PrivacyAndPolicy)!
            
        case .localURL:
            return URL (string: "http://")!
            
        case .stagingURL:
            //https://api.orbitnet.biz/Help/Api/api/Oauth/GetUser?token1={token1}&token2={token2}&token3={token3}
            return URL (string: "https://api.orbitnet.biz/api")!
            
        case .developmentURL:
            // http://192.168.0.131:9235/app/
            return URL (string: "http://")!
            
        case .socketBaseURL:
            return URL (string: "http://128.199.222.145:8000/")!
            
        case .other(let otherURLString):
            return URL (string: otherURLString)!
        }
    }
}

let baseURL: String = StaticEndpoints.stagingURL.url.absoluteString

enum EndPoints {
    
    case login(String,String,String)
    case forgotPassword(String)
    case cryptoWalletData(String,String)
    case dashboardMainData(String,String)
    case dashboardTurnoverData(String,String)

    case getBPReading(String,String,String,String)
    var path: URL {
        
        switch self {
            // Sign up endpoint
        case .login(let email, let password, let token):
            
            return URL (string: baseURL + "/Oauth/GetUser?token1=\(email)&token2=\(password)&token3=\(token)")!
            
        case .forgotPassword(let email):
            
            return URL (string: baseURL + "/Join/GetPassword?email=\(email)")!
            
        case .cryptoWalletData(let userID, let token):
            
            return URL (string: baseURL + "/Values/CoinWALLETData?tokenOth=\(userID)&token=\(token)")!

        case .dashboardMainData(let userID, let token):
            
            return URL (string: baseURL + "/Values/DashboardMainData?tokenOth=\(userID)&token=\(token)")!

        case .dashboardTurnoverData(let userID, let token):
            
            return URL (string: baseURL + "/Values/ACCOUNTTURNOVERData?tokenOth=\(userID)&token=\(token)")!

            
        case .getBPReading(let token, let usrID, let frmDate, let toDate):
                return URL (string: baseURL + "/common/get/chart/data/according/to/user?&access_token=\(token)&patientUserId=\(usrID)&fromdate=\(frmDate)&todate=\(toDate)&dayno=0")!
        }
    }
}

//MARK:- Enum Endpoint Final Path
enum LinksEnum : String {
    case terms = "http://202.157.76.19:9230/app/staticPage/terms"
}
